# ip-blocker



## About project

This test job is called ip-blocker. This is repo for helm chart
- If you looking for development repo use this [link](https://gitlab.com/zhag232/ip-blocker)

## TL;DR

```
git clone https://gitlab.com/zhag232/ip-blocker-helm.git
helm dependency update ip-blocker-helm
helm install my-release ip-blocker-helm
```

## Example installation on microk8s

```
git clone https://gitlab.com/zhag232/ip-blocker-helm.git
helm dependency update ip-blocker-helm
helm install test-app ip-blocker-helm --set ingress.host=test.app.com  --set env.DBPASS=secretpass  --set postgresql.postgresqlPassword=secretpass  --set postgresql.replicationPassword=secretpass  --set postgresql.replication.password=secretpass  --set env.SMTPSERVER=smtp.domain.com  --set ingress.annotations."kubernetes\.io/ingress\.class"="public"
```

## Feathures

- Deploying database Master and Read Replica based on PostgreSQL.
- Deploying a simple python application with functions:
    - It responds to the URL like 'http://host/?n=x' and returns n*n.
    - It responds to the URL 'http://host/blacklisted' with conditions:
    - return error code 444 to the visitor
    - block the IP of the visitor
    - send an email with IP address
    - insert into PostgreSQL table information: path, IP address of the visitor and datetime when he got blocked


## Prerequisites

- Kubernetes 1.12+
- Helm 3.1.0
- Ingress controller
- PV provisioner support in the underlying infrastructure

## Parameters

| Name                                    | Description                                                                | Value                                            |
| --------------------------------------- | ---------------------------------------------------------------------------| ------------------------------------------------ |
| `image`                                 | Docker image                                                               | `"registry.gitlab.com/zhag232/ip-blocker/main"`  |
| `imageTag`                              | Docker image tag                                                           | `"latest"`                                       |
| `replicas`                              | Number of deploymetn replicas                                              | `"1"`                                            |
| `env.HOSTNAME`                          | bind web server to address                                                 | `"0.0.0.0"`                                      |
| `env.SERVERPORT`                        | bind web server to port                                                    | `"8080"`                                         |
| `env.DBNAME`                            | Database name                                                              | `"postgres"`                                     |
| `env.DBUSER`                            | Database user                                                              | `"postgres"`                                     |
| `env.DBPASS`                            | Database user password                                                     | `"mysecretpass10"`                               |
| `env.DBHOST`                            | Database host (by default use installed pgsql)                             | `""`                                             |
| `env.DBPORT`                            | Database port                                                              | `"5432"`                                         |
| `env.SMTPSERVER`                        | SMTP server for sending emails                                             | `"smtp.domain.com"`                              |
| `env.SMTPPORT`                          | SMTP server port                                                           | `"25"`                                           |
| `env.EMAILFROM`                         | Send email from                                                            | `"ip-blocker@domain.com"`                        |
| `env.EMAILTO`                           | Send email to                                                              | `"test@domain.com"`                              |
| `resources.requests.cpu`                | Request CPU                                                                | `"200m"`                                         |
| `resources.requests.memory`             | Request memory                                                             | `"256Mi"`                                        |
| `resources.limits.cpu`                  | limits CPU                                                                 | `"200m"`                                         |
| `resources.limits.memory`               | limits memory                                                              | `"256Mi"`                                        |
| `service.web.app_port`                  | Container port                                                             | `"8080"`                                         |
| `service.web.port`                      | Service port                                                               | `"80"`                                           |
| `ingress.host`                          | Ingress host                                                               | `"ip-blocker.domain.com"`                        |
| `ingress.annotations`                   | Ingress annotations                                                        | `"{}"`                                           |
| `postgresql.postgresqlUsername`         | Database user for pgsql chart                                              | `"postgres"`                                     |
| `postgresql.postgresqlPassword`         | Password database user for pgsql chart                                     | `"mysecretpass10"`                               |
| `postgresql.postgresqlDatabase`         | Database name for pgsql chart                                              | `"postgres"`                                     |
| `postgresql.replicationPassword`        | Replication password for pgsql chart                                       | `"mysecretpass10"`                               |
| `postgresql.volumePermissions.enabled`  | Enable init container and fix permissions                                  | `"true"`                                         |
| `postgresql.replication.readReplicas`   | Number of pgsql read replicas                                              | `"1"`                                            |
| `postgresql.replication.enabled`        | Enable pgsql replication                                                   | `"true"`                                         |
| `postgresql.replication.password`       | Password for  pgsql replication                                            | `"mysecretpass10"`                                         |
 